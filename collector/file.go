package collector

import "path/filepath"

// ---------
// Files
// ---------

type Files []File

func (f Files) Names() []string {
	var names []string
	for _, file := range f {
		names = append(names, file.name)
	}

	return names
}

// ------------
// File
// ------------

type File struct {
	name       string
	extension  string
	fileBefore string
	fileAfter  string
}

func NewFile(name, before, after string) File {
	return File{
		name:       name,
		extension:  filepath.Ext(name),
		fileBefore: before,
		fileAfter:  after,
	}
}

func (f File) Name() string {
	return f.name
}

func (f File) Extension() string {
	return f.extension
}

func (f File) FileBefore() string {
	return f.fileBefore
}

func (f File) FileAfter() string {
	return f.fileAfter
}

func (f File) Deleted() bool {
	return len(f.fileBefore) > 0 && len(f.fileAfter) == 0
}
