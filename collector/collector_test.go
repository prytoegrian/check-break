package collector

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/prytoegrian/check-break/io"
)

// ---------
// Tests
// ---------

func Test_Unit_CollectErrNamesModifiedNoDiff(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := newGitMock()
	git.errModified = io.ErrEmptyDiff
	collector := NewCollector(git)
	// act
	err := collector.Collect()
	// assert
	assert.Nil(err)
}

func Test_Unit_CollectErrNamesModifiedOther(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := newGitMock()
	git.errModified = errors.New("this is an error")
	collector := NewCollector(git)
	// act
	err := collector.Collect()
	// assert
	assert.Equal(git.errModified, err)
}

// We do not duplicate these two tests since we only test influence of GitInteractor interface
func Test_Unit_CollectErrFileStart(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := newGitMock()
	git.modified = []string{"file1", "file2"}
	git.errStart = errors.New("bad file start")
	collector := NewCollector(git)
	// act
	err := collector.Collect()
	// assert
	assert.Equal(git.errStart, err)
}

func Test_Unit_CollectErrFileEnd(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := newGitMock()
	git.modified = []string{"file3", "file4"}
	git.errEnd = errors.New("bad file end")
	collector := NewCollector(git)
	// act
	err := collector.Collect()
	// assert
	assert.Equal(git.errEnd, err)
}

func Test_Unit_CollectErrNamesDeletedNoDiff(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := newGitMock()
	// Bypass modified list
	git.errModified = io.ErrEmptyDiff
	git.errDeleted = io.ErrEmptyDiff
	collector := NewCollector(git)
	// act
	err := collector.Collect()
	// assert
	assert.Nil(err)
}

func Test_Unit_CollectErrNamesDeletedOther(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := newGitMock()
	git.errModified = io.ErrEmptyDiff
	git.errDeleted = errors.New("this is an error on deleted")
	collector := NewCollector(git)
	// act
	err := collector.Collect()
	// assert
	assert.Equal(git.errDeleted, err)
}

func Test_Unit_CollectNoErr(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := newGitMock()
	git.modified = []string{"file5", "file6"}
	git.deleted = []string{"file7", "file8"}
	collector := NewCollector(git)
	// act
	collector.Collect()
	// assert
	lenFilenames := len(append(git.modified, git.deleted...))
	lenFiles := len(collector.Files())
	assert.Equal(lenFilenames, lenFiles)
}

// ----------
// Benchmark
// ----------

func BenchmarkCollectErrNamesModifiedNoDiff(b *testing.B) {
	git := newGitMock()
	git.errModified = io.ErrEmptyDiff
	collector := NewCollector(git)

	for i := 0; i < b.N; i++ {
		collector.Collect()
	}
}

func BenchmarkCollectErrNamesModifiedOther(b *testing.B) {
	git := newGitMock()
	git.errModified = errors.New("this is an error")
	collector := NewCollector(git)

	for i := 0; i < b.N; i++ {
		collector.Collect()
	}
}

func BenchmarkCollectErrFileStart(b *testing.B) {
	git := newGitMock()
	git.modified = []string{"file1", "file2"}
	git.errStart = errors.New("bad file start")
	collector := NewCollector(git)

	for i := 0; i < b.N; i++ {
		collector.Collect()
	}
}

func BenchmarkCollectErrFileEnd(b *testing.B) {
	git := newGitMock()
	git.modified = []string{"file3", "file4"}
	git.errEnd = errors.New("bad file end")
	collector := NewCollector(git)

	for i := 0; i < b.N; i++ {
		collector.Collect()
	}
}

func BenchmarkCollectErrNamesDeletedNoDiff(b *testing.B) {
	git := newGitMock()
	git.errModified = io.ErrEmptyDiff
	git.errDeleted = io.ErrEmptyDiff
	collector := NewCollector(git)

	for i := 0; i < b.N; i++ {
		collector.Collect()
	}
}

func BenchmarkCollectErrNamesDeletedOther(b *testing.B) {
	git := newGitMock()
	git.errModified = io.ErrEmptyDiff
	collector := NewCollector(git)

	for i := 0; i < b.N; i++ {
		collector.Collect()
	}
}

func BenchmarkCollectNoErr(b *testing.B) {
	git := newGitMock()
	git.modified = []string{"file5", "file6"}
	git.deleted = []string{"file7", "file8"}
	collector := NewCollector(git)

	for i := 0; i < b.N; i++ {
		collector.Collect()
	}
}

// -----
// Mock
// -----

type gitMock struct {
	modified    []string
	errModified error
	deleted     []string
	errDeleted  error
	fileStart   string
	errStart    error
	fileEnd     string
	errEnd      error
}

func newGitMock() *gitMock {
	return &gitMock{}
}

func (g *gitMock) NamesModified() ([]string, error) {
	return g.modified, g.errModified
}
func (g *gitMock) NamesDeleted() ([]string, error) {
	return g.deleted, g.errDeleted
}

func (g *gitMock) FileAtStartingPoint(string) (string, error) {
	return g.fileStart, g.errStart
}

func (g *gitMock) FileAtEndingPoint(string) (string, error) {
	return g.fileEnd, g.errEnd
}
