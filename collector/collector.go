package collector

import (
	"runtime"
	"sync"

	"gitlab.com/prytoegrian/check-break/io"
)

// Collection is the very first step of checkbreak. Its builds collector.Files,
// including for each file two stringified versions of the file: one at the first git revision, one at the last.
// Modified and deleted files are fetched differently because there is no version anymore at the ending point.

// The collection is as concurrent as possible: each file gathering is atomic from other so we can run them in concurrence.
// Thus, the collector.Files slice is permutable but it's not important in our context.

type collector struct {
	files Files
	git   io.GitInteractor
}

func NewCollector(g io.GitInteractor) *collector {
	return &collector{git: g}
}

func (c *collector) Collect() error {
	// Benchmark collection of 638 files:
	// - sequential version: 1.07s
	// - concurrence version: 0.43s
	if err := c.collectModified(); err != nil {
		return err
	}

	if err := c.collectDeleted(); err != nil {
		return err
	}

	return nil
}

// collectModified fetches all modified files (and theirs changes) and store them in collector
func (c *collector) collectModified() error {
	names, err := c.git.NamesModified()
	if err == io.ErrEmptyDiff {
		return nil
	}
	if err != nil {
		return err
	}
	// Each goroutine build an entire file (before and after versions),
	// filling either an error chan if something went wrong, or a collector.File chan.
	// Finally, a final goroutine waits for all others to send good ending signal.
	file := make(chan File)
	errC := make(chan error)
	quit := make(chan bool, 1)
	// Using a semaphore prevent from opening len(names) simultaneous goroutines (and hit "too many open file" error)
	// Cf. https://stackoverflow.com/a/38825523
	semaphore := make(chan struct{}, runtime.NumCPU())
	var wg sync.WaitGroup
	wg.Add(len(names))
	for _, name := range names {
		go func(name string) {
			semaphore <- struct{}{}
			defer func() { <-semaphore }()
			defer wg.Done()
			before, errB := c.git.FileAtStartingPoint(name)
			if errB != nil {
				errC <- errB
				return
			}
			after, errA := c.git.FileAtEndingPoint(name)
			if errA != nil {
				errC <- errA
				return
			}

			file <- NewFile(name, before, after)
		}(name)
	}
	go func() {
		wg.Wait()
		quit <- true
	}()
	for {
		select {
		case f := <-file:
			c.files = append(c.files, f)
		case e := <-errC:
			close(file)
			close(quit)
			return e
		case <-quit:
			close(file)
			close(errC)
			return nil
		}
	}
}

// collectDeleted fetches all deleted files (and theirs changes) and store them in collector
func (c *collector) collectDeleted() error {
	names, err := c.git.NamesDeleted()
	if err == io.ErrEmptyDiff {
		return nil
	}
	if err != nil {
		return err
	}
	// Each goroutine build an entire file (here, only before version),
	// filling either an error chan if something went wrong, or a collector.File chan.
	// Finally, a final goroutine waits for all others to send good ending signal.
	file := make(chan File)
	errC := make(chan error)
	quit := make(chan bool, 1)
	// Using a semaphore prevent from opening len(names) simultaneous goroutines (and hit "too many open file" error)
	// Cf. https://stackoverflow.com/a/38825523
	semaphore := make(chan struct{}, runtime.NumCPU())
	var wg sync.WaitGroup
	wg.Add(len(names))
	for _, name := range names {
		go func(name string) {
			semaphore <- struct{}{}
			defer func() { <-semaphore }()
			defer wg.Done()
			before, err := c.git.FileAtStartingPoint(name)
			if err != nil {
				errC <- err
			}
			file <- NewFile(name, before, "")
		}(name)
	}
	go func() {
		wg.Wait()
		quit <- true
	}()
	for {
		select {
		case f := <-file:
			c.files = append(c.files, f)
		case e := <-errC:
			close(file)
			close(quit)
			return e
		case <-quit:
			close(file)
			close(errC)
			return nil
		}
	}
}

func (c *collector) Files() Files {
	return c.files
}
