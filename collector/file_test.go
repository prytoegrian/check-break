package collector

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// File(s) are "entities" so we only test non trivial behaviors, not accessors.

func Test_Unit_Extension(t *testing.T) {
	// arrange
	assert := assert.New(t)
	tests := []struct {
		name     string
		expected string
	}{
		{"file.go", ".go"},
		{"file.java", ".java"},
		{".gitignore", ".gitignore"},
		{"noname", ""},
	}

	for _, test := range tests {
		// act
		file := NewFile(test.name, "", "")
		// assert
		assert.Equal(test.expected, file.Extension())
	}
}

func Test_Unit_Deleted(t *testing.T) {
	// arrange
	assert := assert.New(t)
	tests := []struct {
		name     string
		before   string
		after    string
		expected bool
	}{
		{"newfile.go", "", "new go content", false},
		{"changedfile.php", "old php content", "new php content", false},
		{"deletedfile.py", "old python content", "", true},
		{"nofile.java", "", "", false},
	}

	for _, test := range tests {
		// act
		file := NewFile(test.name, test.before, test.after)
		// assert
		assert.Equal(test.expected, file.Deleted())
	}
}

func Test_Unit_Names(t *testing.T) {
	// arrange
	assert := assert.New(t)
	files := Files{
		NewFile("newfile.go", "", ""),
		NewFile("changedfile.php", "", ""),
		NewFile("deletedfile.py", "", ""),
		NewFile("nofile.java", "", ""),
	}
	names := []string{
		"newfile.go",
		"changedfile.php",
		"deletedfile.py",
		"nofile.java",
	}

	// act
	for i, name := range files.Names() {
		// assert
		assert.Equal(names[i], name)
	}
}
