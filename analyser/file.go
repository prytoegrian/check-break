package analyser

import "gitlab.com/prytoegrian/check-break/parser"

// ----------
// File
// ----------

type Files []file

type file struct {
	name    string
	changes map[analysis]rowsDeltas
}

func newFile(name string) *file {
	return &file{
		name:    name,
		changes: make(map[analysis]rowsDeltas),
	}
}

func (f *file) Name() string {
	return f.name
}

func (f *file) Changes() map[analysis]rowsDeltas {
	return f.changes
}

func (f *file) addChange(a analysis, r rowDelta) {
	f.changes[a] = append(f.changes[a], r)
}

func (f *file) addChanges(a analysis, rr rowsDeltas) {
	f.changes[a] = append(f.changes[a], rr...)
}

func (f *file) hasChanges() bool {
	for _, c := range f.changes {
		if len(c) > 0 {
			return true
		}
	}

	return false
}

// ----------
// RowDelta
// ----------

type analysis string

type rowsDeltas []rowDelta

type rowDelta struct {
	before   string
	after    string
	analysis analysis
}

func newRowDelta(before string, after string, analysis analysis) rowDelta {
	return rowDelta{before, after, analysis}
}

func (r rowDelta) Before() string {
	return r.before
}

func (r rowDelta) After() string {
	return r.after
}

func (r rowDelta) Analysis() analysis {
	return r.analysis
}

type pair struct {
	before parser.Function
	after  parser.Function
}

// ----------
// Pair
// ----------

func newPair(before, after parser.Function) pair {
	return pair{before, after}
}

func (p pair) isParameterDeletion() bool {
	return len(p.before.Parameters()) > len(p.after.Parameters())
}

func (p pair) isParameterRetype() bool {
	if len(p.before.Parameters()) != len(p.after.Parameters()) {
		return false
	}
	afterParameter := p.after.Parameters()
	for i, param := range p.before.Parameters() {
		if param.Type() != afterParameter[i].Type() {
			return true
		}
	}

	return false
}

func (p pair) isParameterAdding() bool {
	return len(p.before.Parameters()) < len(p.after.Parameters())
}

func (p pair) isResultRetype() bool {
	if len(p.before.Results()) != len(p.after.Results()) {
		return false
	}
	afterResult := p.after.Results()
	for i, res := range p.before.Results() {
		if res.Type() != afterResult[i].Type() {
			return true
		}
	}

	return false
}

func (p pair) isResultDeletion() bool {
	return len(p.before.Results()) > len(p.after.Results())
}

func (p pair) isResultAdding() bool {
	return len(p.before.Results()) < len(p.after.Results())
}
