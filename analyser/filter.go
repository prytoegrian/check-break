package analyser

type predicatePair func(pair) bool

func filterPairs(pairs []pair, predicate predicatePair) []pair {
	var filtered []pair
	for _, pair := range pairs {
		if predicate(pair) {
			filtered = append(filtered, pair)
		}
	}

	return filtered
}

type predicateFile func(file) bool

func filterFiles(files Files, predicate predicateFile) Files {
	var filtered Files
	for _, file := range files {
		if predicate(file) {
			filtered = append(filtered, file)
		}
	}

	return filtered
}
