package analyser

import (
	"gitlab.com/prytoegrian/check-break/parser"
)

// Once the parsing step done, parser.Files are analysed regarding to theirs changes. At this step, checkbreak doesn't care anymore of the filetype.
// Analysis step aims to transform them into analyser.Files, and give to each one pairs of changes and an explanation of the change.
// In the context of compatibility break, relevant changes are:
// - function / method deletion,
// - parameter deletion,
// - parameter re-typing,
// - parameter adding without default value,
// - parameter deletion,
// - parameter default value changed,
// - result deletion,
// - result re-typing,
// - result adding.
//
// All function starting with withX() are based on functional programming to filter the slice via a predicate.

// TODO 2022-08-30: what happen if there are multiples changes at the same time?

const A_DELETION analysis = "Function deletion"
const A_PARAM_DELETION analysis = "Parameter deletion"
const A_PARAM_RETYPING analysis = "Parameter re-typing"
const A_PARAM_ADDING analysis = "Parameter adding"
const A_RESULT_DELETION analysis = "Result deletion"
const A_RESULT_RETYPING analysis = "Result re-typing"
const A_RESULT_ADDING analysis = "Result adding"

type analyser struct {
	analysed  Files
	unchanged Files
}

// NewAnalyser creates a new analyser instance.
func NewAnalyser() *analyser {
	return &analyser{}
}

func (a *analyser) Analyse(parsed parser.Files) {
	var analysed Files
	for _, file := range parsed {
		analysed = append(analysed, analysedFile(file))
	}

	a.analysed = filterChangedFiles(analysed)
	a.unchanged = filterUnchangedFiles(analysed)
}

func (a analyser) Files() Files {
	return a.analysed
}

func (a analyser) Unchanged() Files {
	return a.unchanged
}

func analysedFile(file parser.File) file {
	current := newFile(file.Name())
	var pairs []pair
	for _, before := range file.Before() {
		pair := findPair(before, file.After())
		if pair == nil {
			rowDelta := newRowDelta(before.Signature(), "", A_DELETION)
			current.addChange(A_DELETION, rowDelta)
			continue
		}

		pairs = append(pairs, newPair(before, *pair))
	}
	if w := withParameterDeletion(pairs); len(w) > 0 {
		current.addChanges(A_PARAM_DELETION, w)
	}
	if w := withParameterRetype(pairs); len(w) > 0 {
		current.addChanges(A_PARAM_RETYPING, w)
	}
	// TODO 2022-08-29: param added w/o a default value
	if w := withParameterAdding(pairs); len(w) > 0 {
		current.addChanges(A_PARAM_ADDING, withParameterAdding(pairs))
	}
	// TODO 2022-08-29: w/ default parameter removed
	// TODO 2022-08-29: w/ default parameter changed
	if w := withResultDeletion(pairs); len(w) > 0 {
		current.addChanges(A_RESULT_DELETION, w)
	}
	if w := withResultRetype(pairs); len(w) > 0 {
		current.addChanges(A_RESULT_RETYPING, w)
	}
	if w := withResultAdding(pairs); len(w) > 0 {
		current.addChanges(A_RESULT_ADDING, w)
	}

	return *current
}

func findPair(before parser.Function, after parser.Functions) *parser.Function {
	for _, candidate := range after {
		if before.IsPairOf(candidate) {
			return &candidate
		}
	}
	return nil
}

func filterChangedFiles(files Files) Files {
	predicate := func(f file) bool {
		return f.hasChanges()
	}

	return filterFiles(files, predicate)
}

func filterUnchangedFiles(files Files) Files {
	predicate := func(f file) bool {
		return !f.hasChanges()
	}

	return filterFiles(files, predicate)
}
func withParameterDeletion(pairs []pair) rowsDeltas {
	predicate := func(p pair) bool {
		return p.isParameterDeletion()
	}

	filtered := filterPairs(pairs, predicate)
	return pairsToRowsDelta(filtered, A_PARAM_DELETION)
}

func withParameterRetype(pairs []pair) rowsDeltas {
	predicate := func(p pair) bool {
		return p.isParameterRetype()
	}

	filtered := filterPairs(pairs, predicate)
	return pairsToRowsDelta(filtered, A_PARAM_RETYPING)
}

func withParameterAdding(pairs []pair) rowsDeltas {
	predicate := func(p pair) bool {
		return p.isParameterAdding()
	}

	filtered := filterPairs(pairs, predicate)
	return pairsToRowsDelta(filtered, A_PARAM_ADDING)
}

func withResultRetype(pairs []pair) rowsDeltas {
	predicate := func(p pair) bool {
		return p.isResultRetype()
	}

	filtered := filterPairs(pairs, predicate)
	return pairsToRowsDelta(filtered, A_RESULT_RETYPING)
}

func withResultDeletion(pairs []pair) rowsDeltas {
	predicate := func(p pair) bool {
		return p.isResultDeletion()
	}

	filtered := filterPairs(pairs, predicate)
	return pairsToRowsDelta(filtered, A_RESULT_DELETION)
}

func withResultAdding(pairs []pair) rowsDeltas {
	predicate := func(p pair) bool {
		return p.isResultAdding()
	}

	filtered := filterPairs(pairs, predicate)
	return pairsToRowsDelta(filtered, A_RESULT_ADDING)
}

func pairsToRowsDelta(pairs []pair, analysis analysis) rowsDeltas {
	var rows rowsDeltas
	for _, p := range pairs {
		row := newRowDelta(p.before.Signature(), p.after.Signature(), analysis)
		rows = append(rows, row)
	}

	return rows
}
