package analyser

import (
	"testing"

	"gitlab.com/prytoegrian/check-break/parser"
	"github.com/stretchr/testify/assert"
)

func Test_Unit_AnalyseNoChange(t *testing.T) {
	// arrange
	assert := assert.New(t)
	analyser := NewAnalyser()
	functions := parser.Functions{parser.NewFunction("Foo", nil, nil, nil)}
	sources := parser.Files{
		parser.NewFile("myfile", functions, functions),
	}
	// act
	analyser.Analyse(sources)
	// assert
	assert.Empty(analyser.Files())
}

func Test_Unit_AnalyseDeletion(t *testing.T) {
	// arrange
	assert := assert.New(t)
	analyser := NewAnalyser()
	functions := parser.Functions{parser.NewFunction("Foo", nil, nil, nil)}

	sources := parser.Files{
		parser.NewFile("myfile", functions, parser.Functions{}),
	}
	// act
	analyser.Analyse(sources)
	files := analyser.Files()
	// assert
	assert.Len(files, 1)
	for _, f := range files {
		for a := range f.Changes() {
			assert.Equal(A_DELETION, a)
		}
	}
}

// -------
// Param
// -------

func Test_Unit_AnalyseParamDeletion(t *testing.T) {
	// arrange
	assert := assert.New(t)
	analyser := NewAnalyser()
	parameters := parser.Parameters{parser.NewParameter("string")}
	before := parser.Functions{parser.NewFunction("Foo", nil, parameters, nil)}
	after := parser.Functions{parser.NewFunction("Foo", nil, parser.Parameters{}, nil)}
	sources := parser.Files{
		parser.NewFile("myfile", before, after),
	}
	// act
	analyser.Analyse(sources)
	files := analyser.Files()
	// assert
	assert.Len(files, 1)
	for _, f := range files {
		for a := range f.Changes() {
			assert.Equal(A_PARAM_DELETION, a)
		}
	}
}

func Test_Unit_AnalyseParamReTyping(t *testing.T) {
	// arrange
	assert := assert.New(t)
	analyser := NewAnalyser()
	parametersBefore := parser.Parameters{parser.NewParameter("string")}
	parametersAfter := parser.Parameters{parser.NewParameter("bool")}
	before := parser.Functions{parser.NewFunction("Foo", nil, parametersBefore, nil)}
	after := parser.Functions{parser.NewFunction("Foo", nil, parametersAfter, nil)}
	sources := parser.Files{
		parser.NewFile("myfile", before, after),
	}
	// act
	analyser.Analyse(sources)
	files := analyser.Files()
	// assert
	assert.Len(files, 1)
	for _, f := range files {
		for a := range f.Changes() {
			assert.Equal(A_PARAM_RETYPING, a)
		}
	}
}

func Test_Unit_AnalyseParamAdding(t *testing.T) {
	// arrange
	assert := assert.New(t)
	analyser := NewAnalyser()
	parametersBefore := parser.Parameters{parser.NewParameter("string")}
	parametersAfter := parser.Parameters{parser.NewParameter("string"), parser.NewParameter("bool")}
	before := parser.Functions{parser.NewFunction("Foo", nil, parametersBefore, nil)}
	after := parser.Functions{parser.NewFunction("Foo", nil, parametersAfter, nil)}
	sources := parser.Files{
		parser.NewFile("myfile", before, after),
	}
	// act
	analyser.Analyse(sources)
	files := analyser.Files()
	// assert
	assert.Len(files, 1)
	for _, f := range files {
		for a := range f.Changes() {
			assert.Equal(A_PARAM_ADDING, a)
		}
	}
}

// ---------
// Result
// ---------

func Test_Unit_AnalyseResultDeletion(t *testing.T) {
	// arrange
	assert := assert.New(t)
	analyser := NewAnalyser()
	resultsBefore := parser.Results{parser.NewResult("string")}
	before := parser.Functions{parser.NewFunction("Foo", nil, nil, resultsBefore)}
	after := parser.Functions{parser.NewFunction("Foo", nil, nil, parser.Results{})}
	sources := parser.Files{
		parser.NewFile("myfile", before, after),
	}
	// act
	analyser.Analyse(sources)
	files := analyser.Files()
	// assert
	assert.Len(files, 1)
	for _, f := range files {
		for a := range f.Changes() {
			assert.Equal(A_RESULT_DELETION, a)
		}
	}
}

func Test_Unit_AnalyseResultReTyping(t *testing.T) {
	// arrange
	assert := assert.New(t)
	analyser := NewAnalyser()
	resultsBefore := parser.Results{parser.NewResult("string")}
	resultsAfter := parser.Results{parser.NewResult("bool")}
	before := parser.Functions{parser.NewFunction("Foo", nil, nil, resultsBefore)}
	after := parser.Functions{parser.NewFunction("Foo", nil, nil, resultsAfter)}
	sources := parser.Files{
		parser.NewFile("myfile", before, after),
	}
	// act
	analyser.Analyse(sources)
	files := analyser.Files()
	// assert
	assert.Len(files, 1)
	for _, f := range files {
		for a := range f.Changes() {
			assert.Equal(A_RESULT_RETYPING, a)
		}
	}
}

func Test_Unit_AnalyseResultAdding(t *testing.T) {
	// arrange
	assert := assert.New(t)
	analyser := NewAnalyser()
	resultsBefore := parser.Results{parser.NewResult("string")}
	resultsAfter := parser.Results{parser.NewResult("string"), parser.NewResult("bool")}
	before := parser.Functions{parser.NewFunction("Foo", nil, nil, resultsBefore)}
	after := parser.Functions{parser.NewFunction("Foo", nil, nil, resultsAfter)}
	sources := parser.Files{
		parser.NewFile("myfile", before, after),
	}
	// act
	analyser.Analyse(sources)
	files := analyser.Files()
	// assert
	assert.Len(files, 1)
	for _, f := range files {
		for a := range f.Changes() {
			assert.Equal(A_RESULT_ADDING, a)
		}
	}
}

// multiples changes at a time
