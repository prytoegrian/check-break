package parser

import (
	"gitlab.com/prytoegrian/check-break/collector"
)

type predicateCollector func(collector.File) bool

func filterCollector(sources collector.Files, predicate predicateCollector) []collector.File {
	var filtered collector.Files
	for _, current := range sources {
		if predicate(current) {
			filtered = append(filtered, current)
		}
	}

	return filtered
}

type predicateFile func(File) bool

func filterFile(sources Files, predicate predicateFile) Files {
	var filtered Files
	for _, current := range sources {
		if predicate(current) {
			filtered = append(filtered, current)
		}
	}

	return filtered
}
