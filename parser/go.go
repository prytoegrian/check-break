package parser

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
)

type golangParser struct{}

func NewGolangParser() golangParser {
	return golangParser{}
}

func (g golangParser) ParseFile(file string) (Functions, error) {
	fset := token.NewFileSet()
	f, err := parser.ParseFile(fset, "", file, parser.AllErrors)
	if err != nil {
		return nil, err
	}

	var functions Functions
	for _, decl := range f.Decls {
		// Only exported function declarations
		if fn, ok := decl.(*ast.FuncDecl); ok {
			if fn.Name.IsExported() {
				candidate := g.parseFunction(fn)
				functions = append(functions, candidate)
			}
		}
	}

	return functions, nil
}

func (g golangParser) parseFunction(fn *ast.FuncDecl) Function {
	name := fn.Name.Name
	obj := g.parseReceiver(fn)
	params := g.parseParameters(fn.Type)
	results := g.parseResults(fn.Type)
	return NewFunction(name, obj, params, results)
}

func (g golangParser) parseReceiver(fn *ast.FuncDecl) *object {
	if fn.Recv != nil {
		for _, field := range fn.Recv.List {
			obj := newObject(g.getExprType(field.Type))
			return &obj
		}
	}

	return nil
}

func (g golangParser) parseParameters(funcType *ast.FuncType) Parameters {
	var parameters Parameters
	if funcType.Params.List != nil {
		for _, field := range funcType.Params.List {
			fieldType := g.getExprType(field.Type)
			// Usually, there are 1 to N variables names for a given type.
			// But, function as parameter / result allow anonymous internal parameter
			// In this last case, consider 0 name as 1
			if len(field.Names) == 0 {
				param := NewParameter(fieldType)
				parameters = append(parameters, param)
				continue
			}
			for range field.Names {
				param := NewParameter(fieldType)
				parameters = append(parameters, param)
			}
		}
	}

	return parameters
}

func (g golangParser) parseResults(funcType *ast.FuncType) Results {
	var results Results
	if funcType.Results != nil {
		for _, field := range funcType.Results.List {
			res := NewResult(g.getExprType(field.Type))
			results = append(results, res)
		}
	}

	return results
}

// getExprType returns the real type of a field
// Based on Go syntax, here are the possibilities:
// - local type (cf. https://pkg.go.dev/go/ast#pkg-types),
// - pointer to this local type,
// - imported type (ex: pkg.type)
// - pointer to this imported type
func (g golangParser) getExprType(expression ast.Expr) string {
	switch t := expression.(type) {
	case *ast.Ident:
		return t.Name
	case *ast.StarExpr:
		return "*" + g.getExprType(t.X)
	case *ast.SelectorExpr:
		return g.getExprType(t.X) + "." + t.Sel.Name
	case *ast.ArrayType:
		return "[]" + g.getExprType(t.Elt)
	case *ast.MapType:
		return fmt.Sprintf("map[%s]%s", g.getExprType(t.Key), g.getExprType(t.Value))
	case *ast.InterfaceType:
		return "interface{}"
	case *ast.ChanType:
		return g.chanSyntax(t) + " " + g.getExprType(t.Value)
	case *ast.FuncType:
		params := g.parseParameters(t)
		results := g.parseResults(t)
		function := NewFunction("", nil, params, results)
		return function.Signature()
	case *ast.Ellipsis:
		return "..." + g.getExprType(t.Elt)
	default:
		//fmt.Printf("\ndunno: %v, %T", t, t)
		return ">unknown<"
	}
}

func (g golangParser) chanSyntax(c *ast.ChanType) string {
	if c.Dir == ast.SEND {
		return "chan<-"
	}
	return "<-chan"
}
