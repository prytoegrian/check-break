package parser

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// File(s) are "entities" so we only test non trivial behaviors, not accessors.

func Test_Unit_Signature(t *testing.T) {
	// arrange
	assert := assert.New(t)
	my := newObject("my")
	// act
	tests := []struct {
		f        Function
		expected string
	}{
		{NewFunction("foo", nil, nil, nil), "func foo() ()"},
		{NewFunction("foo", &my, nil, nil), "func (my) foo() ()"},
	}
	// assert
	for _, test := range tests {
		assert.Equal(test.expected, test.f.Signature())
	}
}

func Test_Unit_IsPairMethod(t *testing.T) {
	// arrange
	assert := assert.New(t)
	obj1 := newObject("obj1")
	obj2 := newObject("obj2")
	reference := NewFunction("foo", &obj1, nil, nil)
	tests := []struct {
		candidate Function
		expected  bool
	}{
		{NewFunction("bar", &obj1, nil, nil), false},
		{NewFunction("baz", &obj2, nil, nil), false},
		{NewFunction("foo", &obj2, nil, nil), false},
		{NewFunction("foo", &obj1, nil, nil), true},
	}
	for _, test := range tests {
		// act
		res := reference.IsPairOf(test.candidate)
		// assert
		assert.Equal(test.expected, res)
	}
}

func Test_Unit_IsPairFunction(t *testing.T) {
	// arrange
	assert := assert.New(t)
	reference := NewFunction("foo", nil, nil, nil)
	tests := []struct {
		candidate Function
		expected  bool
	}{
		{NewFunction("bar", nil, nil, nil), false},
		{NewFunction("foo", nil, nil, nil), true},
	}
	for _, test := range tests {
		// act
		res := reference.IsPairOf(test.candidate)
		// assert
		assert.Equal(test.expected, res)
	}
}
