package parser

import (
	"errors"
	"sync"

	"gitlab.com/prytoegrian/check-break/collector"
)

// Parser step is the main feature of checkbreak. Once the collection step done, this package parse both file versions
// and builds two slices of public functions: one in the old one, one in the new one.
// To this purpose, the filetype (based on extension) helps to select which one of language parser is relevant.
// If no language parser is found, file is listed in parser.Unparseables().

// The parsing is as concurrent as possible: each file transformation is atomic from other so we can run them in concurrence.
// Thus, the analyser.Files slice is permutable but it's not important in our context.

// To add a new language parser, the developper is required to:
// 1. create a new file parser/[language].go,
// 2. create a struct [language]Parser implementing the parser.typeParser interface and its exported constructor (cf. parser.typeParser),
// 3. explode the stringified file to find every public functions w/ name, parameters, results, ...
// 4. add as many tests as possible (I don't care about 100% code coverage but no PR without test will be accepted),
// 5. add the constructor mapped to the file extension to parser._supportedTypes()

// In doubt, cf. parser/go.go

// -----------------
// CollectionParser
// -----------------

type collectionParser struct{}

func NewCollectionParser() collectionParser {
	return collectionParser{}
}

func (cp collectionParser) Parse(source collector.Files) (Files, Files, error) {
	// Benchmark parsing of 638 files:
	// - sequential version: 1.05s
	// - concurrence version: 0.6s
	supported := supportedFiles(source)

	// Each goroutine parses an entire file relatively to its type (before and after versions),
	// filling either an error chan if something went wrong, or an analyser.File chan.
	// Finally, a final goroutine waits for all others to send good ending signal.
	var parsed []File
	fileC := make(chan File)
	errC := make(chan error)
	quit := make(chan bool, 1)
	var wg sync.WaitGroup
	wg.Add(len(supported))
	for _, f := range supported {
		go func(f collector.File) {
			defer wg.Done()
			fileParser := newFileParser()
			file, err := fileParser.parse(f)
			if err != nil {
				errC <- err
				return
			}
			fileC <- *file

		}(f)
	}
	go func() {
		wg.Wait()
		quit <- true
	}()
	for {
		select {
		case f := <-fileC:
			parsed = append(parsed, f)
		case err := <-errC:
			close(fileC)
			close(quit)
			return nil, nil, err
		case <-quit:
			close(errC)
			close(fileC)
			// Exclude files without rowDelta
			return filterFilledFiles(parsed), filterEmptyFiles(parsed), nil
		}
	}
}

func (cp collectionParser) Unparseables(source collector.Files) collector.Files {
	return ignoredFiles(source)
}

// supportedFiles filters the types defined as exploitable
func supportedFiles(sources collector.Files) collector.Files {
	predicate := func(file collector.File) bool {
		return supportedType(file.Extension())
	}

	return filterCollector(sources, predicate)
}

// ignoredFiles filters the types defined as non-exploitable (inverse of SupportedFiles)
func ignoredFiles(sources collector.Files) collector.Files {
	predicate := func(file collector.File) bool {
		return !supportedType(file.Extension())
	}

	return filterCollector(sources, predicate)
}

func filterFilledFiles(files Files) Files {
	predicate := func(f File) bool {
		return !f.empty()
	}

	return filterFile(files, predicate)
}

func filterEmptyFiles(files Files) Files {
	predicate := func(f File) bool {
		return f.empty()
	}

	return filterFile(files, predicate)
}

// -----------------
// FileParser
// -----------------

type fileParser struct{}

func newFileParser() fileParser {
	return fileParser{}
}

func (fp fileParser) parse(source collector.File) (*File, error) {
	// No error checking since we are already sure that type is supported
	typeParser, _ := getTypeParser(source.Extension())
	before, err := typeParser.ParseFile(source.FileBefore())
	if err != nil {
		return nil, err
	}

	var after Functions
	if !source.Deleted() {
		after, err = typeParser.ParseFile(source.FileAfter())
		if err != nil {
			return nil, err
		}
	}

	file := NewFile(source.Name(), before, after)

	return &file, nil
}

// -----------------
// TypeParser
// -----------------

// typeParser represents a parser for a specific language.
// Since all language parsers lie is the same package as parser.collectionParser,
// it's not strictly necessary to export the methods. But to guarantee testability
// of these very sensible files, we export them.
// Note to developper: if you have any better idea, all help are welcome.
type typeParser interface {
	// ParseFile parses a stringified file content and returns parser.Functions for public functions
	ParseFile(string) (Functions, error)
}

// ---------------
// Supported types
// ---------------

func supportedType(filetype string) bool {
	_, ok := _supportedTypes()[filetype]
	return ok
}

func getTypeParser(filetype string) (typeParser, error) {
	parser, ok := _supportedTypes()[filetype]
	if !ok {
		return nil, errors.New("Unknown filetype: " + filetype)
	}

	return parser, nil
}

func _supportedTypes() map[string]typeParser {
	return map[string]typeParser{
		".go": NewGolangParser(),
	}
}
