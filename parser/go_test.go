package parser

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// A great majority of tests lie *Param* and are not duplicated in receiver or results,
// even if theses cases are syntactically possible. We only test type of cases.

// --------
// Common
// --------

func Test_Unit_ParseFile_Err(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main
func main() {
	println("Hello, World!")
`
	// act
	_, err := parser.ParseFile(src)
	// assert
	assert.Error(err)
}

func Test_Unit_ParseFile_NoExported(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main
func main() {
	println("Hello, World!")
}
`
	// act
	f, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	assert.Empty(f)
}

func Test_Unit_ParseFile_MultiplesExported(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main
func Foo() {
	println("Hello, World!")
}
func Bar() {}
`
	// act
	f, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	assert.Len(f, 2)
}

// ---------
// Receiver
// ---------

func Test_Unit_ParseFile_NoRecv(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main
func Foo() {}
`
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.False(f.isMethod())
	}
}

func Test_Unit_ParseFile_WithRecv(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main

type my struct {}
func (my) Foo() {}
`
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Equal("my", f.object.Type())
	}
}

// --------
// Param
// --------

func Test_Unit_ParseFile_NoParam(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main
func Foo() {}
`
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Empty(f.parameters)
	}
}

func Test_Unit_ParseFile_CombinedParams(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main
func Foo(a, b string) {}
`
	expected := []string{"string", "string"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.parameters, 2)
		for i, p := range f.parameters {
			assert.Equal(expected[i], p.Type())
		}
	}
}

func Test_Unit_ParseFile_AnonymousParam(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main
func Foo(bool) {}
`
	expected := []string{"bool"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.parameters, 1)
		for i, p := range f.parameters {
			assert.Equal(expected[i], p.Type())
		}
	}
}

func Test_Unit_ParseFile_ScalarParam(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main
func Foo(a bool) {}
`
	expected := []string{"bool"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.parameters, 1)
		for i, p := range f.parameters {
			assert.Equal(expected[i], p.Type())
		}
	}
}

func Test_Unit_ParseFile_PointerParam(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main
func Foo(a *bool) {}
`
	expected := []string{"*bool"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.parameters, 1)
		for i, p := range f.parameters {
			assert.Equal(expected[i], p.Type())
		}
	}
}

func Test_Unit_ParseFile_LocalStructParam(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main

type my struct {}
func Foo(a my) {}
`
	expected := []string{"my"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.parameters, 1)
		for i, p := range f.parameters {
			assert.Equal(expected[i], p.Type())
		}
	}
}

func Test_Unit_ParseFile_RemoteStructParam(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main

func Foo(a ast.Expr) {}
`
	expected := []string{"ast.Expr"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.parameters, 1)
		for i, p := range f.parameters {
			assert.Equal(expected[i], p.Type())
		}
	}
}

func Test_Unit_ParseFile_ChanRecvParam(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main

func Foo(a <-chan string) {}
`
	expected := []string{"<-chan string"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.parameters, 1)
		for i, p := range f.parameters {
			assert.Equal(expected[i], p.Type())
		}
	}
}

func Test_Unit_ParseFile_ChanSendParam(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main

func Foo(a chan<- string) {}
`
	expected := []string{"chan<- string"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.parameters, 1)
		for i, p := range f.parameters {
			assert.Equal(expected[i], p.Type())
		}
	}
}

func Test_Unit_ParseFile_SliceParam(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main

func Foo(a []string) {}
`
	expected := []string{"[]string"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.parameters, 1)
		for i, p := range f.parameters {
			assert.Equal(expected[i], p.Type())
		}
	}
}

func Test_Unit_ParseFile_MapParam(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main

func Foo(a map[int]string) {}
`
	expected := []string{"map[int]string"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.parameters, 1)
		for i, p := range f.parameters {
			assert.Equal(expected[i], p.Type())
		}
	}
}

func Test_Unit_ParseFile_InterfaceParam(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main

func Foo(a interface{}) {}
`
	expected := []string{"interface{}"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.parameters, 1)
		for i, p := range f.parameters {
			assert.Equal(expected[i], p.Type())
		}
	}
}

func Test_Unit_ParseFile_FunctionParam(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main

func Foo(a func(string) bool) {}
`
	expected := []string{"func (string) (bool)"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.parameters, 1)
		for i, p := range f.parameters {
			assert.Equal(expected[i], p.Type())
		}
	}
}

func Test_Unit_ParseFile_MultiplesParams(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main

func Foo(a string, b, c bool, d int8) {}
`
	expected := []string{"string", "bool", "bool", "int8"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.parameters, 4)
		for i, p := range f.parameters {
			assert.Equal(expected[i], p.Type())
		}
	}
}

// --------
// Result
// --------

func Test_Unit_ParseFile_NoResult(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main
func Foo() {}
`
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Empty(f.results)
	}
}

func Test_Unit_ParseFile_OneResult(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main
func Foo() bool {}
`
	expected := []string{"bool"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.results, 1)
		for i, r := range f.results {
			assert.Equal(expected[i], r.Type())
		}
	}
}

func Test_Unit_ParseFile_MultiplesResults(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewGolangParser()
	src := `
package main
func Foo() (bool, error) {}
`
	expected := []string{"bool", "error"}
	// act
	functions, err := parser.ParseFile(src)
	// assert
	assert.Nil(err)
	for _, f := range functions {
		assert.Len(f.results, 2)
		for i, r := range f.results {
			assert.Equal(expected[i], r.Type())
		}
	}
}
