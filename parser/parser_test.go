package parser

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/prytoegrian/check-break/collector"
)

// Note: there are no mocks here since source files are injectables.
// So, we are allowed to build them from scratch

func Test_Unit_ParseErrParse(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewCollectionParser()
	files := collector.Files{
		collector.NewFile("name.go", "", ""),
	}
	// act
	_, _, err := parser.Parse(files)
	// assert
	assert.Error(err)
}

func Test_Unit_UnknownType(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewCollectionParser()
	file := collector.NewFile("name.xyz", "", "")
	files := collector.Files{
		file,
	}
	// act
	// TODO: test emptyFiles
	_, _, err := parser.Parse(files)
	unparsed := parser.Unparseables(files)
	// assert
	assert.Nil(err)
	for _, u := range unparsed {
		assert.Equal(file, u)
	}
}

func Test_Unit_NoFunctionFile(t *testing.T) {
	// arrange
	assert := assert.New(t)
	parser := NewCollectionParser()
	before := `
package main
func main() {}
`
	after := `
package main
func foo() {}
`
	file := collector.NewFile("name.go", before, after)
	files := collector.Files{
		file,
	}
	// act
	res, _, err := parser.Parse(files)
	// assert
	assert.Nil(err)
	assert.Empty(res)
}
