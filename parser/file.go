package parser

import (
	"fmt"
	"strings"
)

// -----------
// File
// -----------

type Files []File

type File struct {
	name   string
	before Functions
	after  Functions
}

func NewFile(n string, b Functions, a Functions) File {
	return File{
		name:   n,
		before: b,
		after:  a,
	}
}

func (f File) Name() string {
	return f.name
}

func (f File) empty() bool {
	return len(f.before) == 0
}

func (f File) Before() Functions {
	return f.before
}

func (f File) After() Functions {
	return f.after
}

// --------------
// Functions
// --------------

type Functions []Function

// Function is a language agnostic representation of a function
// Here, a function is defined by a name, a parent object (if eligible), a list of parameters and a list of results
type Function struct {
	name       string
	object     *object
	parameters Parameters
	results    Results
}

func NewFunction(name string, obj *object, param Parameters, res Results) Function {
	return Function{
		name:       name,
		object:     obj,
		parameters: param,
		results:    res,
	}
}

func (f Function) isMethod() bool {
	return f.object != nil
}

func (f Function) Name() string {
	return f.name
}

func (f Function) Parameters() Parameters {
	return f.parameters
}

func (f Function) Results() Results {
	return f.results
}

// TODO 2022-08-30: different signature per langage
func (f Function) Signature() string {
	if f.isMethod() {
		return f.signatureMethod()
	}
	return f.signatureFunction()
}

func (f Function) signatureMethod() string {
	pattern := "func (%s) %s(%s) (%s)"
	params := reduceParameters(f.parameters)
	results := reduceResults(f.results)

	return fmt.Sprintf(pattern, f.object.Type(), f.name, params, results)
}

func (f Function) signatureFunction() string {
	pattern := "func %s(%s) (%s)"
	params := reduceParameters(f.parameters)
	results := reduceResults(f.results)

	return fmt.Sprintf(pattern, f.name, params, results)
}

func (f Function) IsPairOf(candidate Function) bool {
	if f.isMethod() && candidate.isMethod() {
		return f.name == candidate.name && f.object.vType == candidate.object.vType
	}
	return f.name == candidate.name && f.object == nil && candidate.object == nil
}

func reduceParameters(params Parameters) string {
	var all []string
	for _, param := range params {
		all = append(all, param.vType)
	}

	return strings.Join(all, ", ")
}

func reduceResults(res Results) string {
	var all []string
	for _, r := range res {
		all = append(all, r.vType)
	}

	return strings.Join(all, ", ")
}

// ----------------------------------------
// Params, Objects, Results (ie. variables)
// ----------------------------------------

type Parameters []parameter
type parameter variable

func NewParameter(vType string) parameter {
	return parameter{vType}
}

func (p parameter) Type() string {
	return p.vType
}

type Results []result
type result variable

func NewResult(vType string) result {
	return result{vType}
}

func (r result) Type() string {
	return r.vType
}

type object variable

func newObject(vType string) object {
	return object{vType}
}

func (o object) Type() string {
	return o.vType
}

// variable is a language variable.
// We don't care about the formal name so the variable is only defined by its type
type variable struct {
	vType string
}
