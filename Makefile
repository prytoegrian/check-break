# Functions

# make_version
# params : $(1) version
#
define make_version
	@semver inc $(1)
	@echo "New release: `semver tag`"
	@git add .semver
	@git commit -m "Releasing `semver tag`"
	@git tag -a `semver tag` -m "Releasing `semver tag`"
endef

default : build

build: unit_test
	go build -o checkbreak ./cmd

major:
	$(call make_version,major)

minor:
	$(call make_version,minor)

patch:
	$(call make_version,patch)

unit_test:
	go test -cover -run _Unit_ ./...

integration_test:
	go test -cover -v -run _Integration_ ./...

bench:
	go test -bench=. -benchmem -run Benchmark -v ./...
