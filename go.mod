module gitlab.com/prytoegrian/check-break

go 1.16

require (
	github.com/fatih/color v1.13.0
	github.com/stretchr/testify v1.8.0
)
