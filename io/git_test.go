package io

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

// ---------
// Tests
// ---------

func Test_Integration_Git(t *testing.T) {
	setup()
	t.Run("test_integration_NamesModifiedBadPoints", test_integration_NamesModifiedBadPoints)
	t.Run("test_integration_NamesModifiedEmpty", test_integration_NamesModifiedEmpty)
	t.Run("test_integration_NamesModifiedWithList", test_integration_NamesModifiedWithList)
	t.Run("test_integration_NamesDeletedWithList", test_integration_NamesDeletedWithList)
	t.Run("test_integration_FileAtStartErr", test_integration_FileAtStartErr)
	t.Run("test_integration_FileAtStartOK", test_integration_FileAtStartOK)
	t.Run("test_integration_FileAtEndOK", test_integration_FileAtEndOK)
	t.Run("test_integration_FileAtEndDeleted", test_integration_FileAtEndDeleted)
	teardown()
}

func test_integration_NamesModifiedBadPoints(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := NewGit("h", "o")
	// act
	_, err := git.NamesModified()
	// assert
	assert.Error(err)
	assert.NotEqual(ErrEmptyDiff, err)
}

func test_integration_NamesModifiedEmpty(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := NewGit("HEAD", "HEAD")
	// act
	_, err := git.NamesModified()
	// assert
	assert.Equal(ErrEmptyDiff, err)
}

func test_integration_NamesModifiedWithList(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := NewGit("HEAD^", "HEAD")
	expected := []string{"file1"}
	// act
	names, _ := git.NamesModified()
	// assert
	assert.Len(names, 1)
	for i, name := range names {
		assert.Equal(expected[i], name)
	}
}

func test_integration_NamesDeletedWithList(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := NewGit("HEAD^", "HEAD")
	expected := []string{"file2"}
	// act
	names, _ := git.NamesDeleted()
	// assert
	assert.Len(names, 1)
	for i, name := range names {
		assert.Equal(expected[i], name)
	}
}

func test_integration_FileAtStartErr(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := NewGit("ii", "HEAD")
	// act
	_, err := git.FileAtStartingPoint("file1")
	// assert
	assert.Error(err)
}

func test_integration_FileAtStartOK(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := NewGit("HEAD^", "HEAD")
	// act
	file, err := git.FileAtStartingPoint("file1")
	// assert
	assert.Nil(err)
	assert.Equal(src, file)
}

func test_integration_FileAtEndOK(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := NewGit("HEAD^", "HEAD")
	// act
	file, err := git.FileAtEndingPoint("file1")
	// assert
	assert.Nil(err)
	assert.Equal(src+another, file)
}

func test_integration_FileAtEndDeleted(t *testing.T) {
	// arrange
	assert := assert.New(t)
	git := NewGit("HEAD^", "HEAD")
	// act
	file, err := git.FileAtEndingPoint("file2")
	// assert
	assert.Error(err)
	assert.Empty(file)
}

// ----------
// Benchmark
// ----------

func BenchmarkGit(b *testing.B) {
	setup()
	b.Run("benchmark_NamesModifiedBadPoints", benchmark_NamesModifiedBadPoints)
	b.Run("benchmark_NamesModifiedWithList", benchmark_NamesModifiedWithList)
	teardown()
}

func benchmark_NamesModifiedBadPoints(b *testing.B) {
	git := NewGit("h", "o")

	for i := 0; i < b.N; i++ {
		git.NamesModified()
	}
}

func benchmark_NamesModifiedWithList(b *testing.B) {
	git := NewGit("HEAD^", "HEAD")

	for i := 0; i < b.N; i++ {
		git.NamesModified()
	}
}

// --------
// Tools
// -------

var oldDir string
var tempDir string
var src = `package main
func main() {
	println("Hello, World!")
}
`
var another = `
func another() {
	println("Hello, World!")
}
`

// return tmpdir or error
func setup() {
	// Do something here.
	fmt.Print("> Integration test setup\n")
	oldDir, _ = os.Getwd()
	var err error
	tempDir, err = os.MkdirTemp("", "test_repo")
	if err != nil {
		log.Fatalf("error creating MkdirTemp: %v", nil)
	}

	fmt.Printf("|> move dir from %s to %v\n", oldDir, tempDir)
	os.Chdir(tempDir)
	fmt.Println("|> initialise a git repo")
	if _, err := run("git init"); err != nil {
		log.Fatalf("error git init: %v", nil)
	}

	fmt.Println("|> add some test files")
	file1 := filepath.Join(tempDir, "file1")
	writeFile(file1, src)
	file2 := filepath.Join(tempDir, "file2")
	writeFile(file2, src)
	fmt.Println("|> commit them")
	if _, err := run("git add ."); err != nil {
		log.Fatalf("error git add: %v", nil)
	}
	if _, err := run("git commit -m \"init\""); err != nil {
		log.Fatalf("error git commit: %v", nil)
	}
	fmt.Println("|> alter file1")
	writeFile(file1, another)
	fmt.Println("|> delete file2")
	if err := os.Remove(file2); err != nil {
		log.Fatalf("error delete: %v", nil)
	}
	fmt.Println("|> commit these changes")
	if _, err := run("git add ."); err != nil {
		log.Fatalf("error git add 2: %v", nil)
	}
	if _, err := run("git commit -m \"last\""); err != nil {
		log.Fatalf("error git commit 2: %v", nil)
	}
	fmt.Print("> Setup completed\n")
}

func writeFile(fullname string, data string) {
	// If the file doesn't exist, create it, or append to the file
	f, err := os.OpenFile(fullname, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	if _, err := f.Write([]byte(data)); err != nil {
		log.Fatal(err)
	}
	if err := f.Close(); err != nil {
		log.Fatal(err)
	}
}

func teardown() {
	os.RemoveAll(tempDir)
	os.Chdir(oldDir)
	fmt.Print("> Teardown completed\n")
}
