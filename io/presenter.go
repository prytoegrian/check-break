package io

import (
	"fmt"
	"strings"

	"github.com/fatih/color"
)

// PrintTitle prints on CLI software title
func PrintTitle(value string) {
	value = "/ " + value
	colorised := color.CyanString(value)
	lenValue := len(value)
	fmt.Println(" " + color.CyanString(strings.Repeat("-", lenValue)))
	fmt.Printf("%s", colorised)
	fmt.Println()
	fmt.Print(color.CyanString(strings.Repeat("-", lenValue+1)))
	fmt.Println()
}

// PrintHeader prints on CLI main help
func PrintHeader() {
	details := `
Checkbreak helps you to track compatibility breaks in your code, and improve decision-making to determine if a new major version is required.
For details about compatibility breaks, head to https://github.com/Prytoegrian/check-break#what-is-a-compatibility-break-.

Remember that parameter retyping is *NOT* a CB if new type is a super type of the previous, and result retyping is *NOT* a CB is new type is a subtype of the previous.

Thanks for using my tool.
`
	fmt.Println(details)
}

// PrintSection prints on CLI a files section
func PrintSection(value string) {
	value = "/ " + value
	colorised := color.CyanString(value)
	lenValue := len(value)
	fmt.Println(" " + color.CyanString(strings.Repeat("-", lenValue)))
	fmt.Printf("%s", colorised)
	fmt.Println()
}

// PrintFile prints on CLI a filename
func PrintFile(value string) {
	fmt.Printf("\\_%s", value)
	fmt.Println()
}

func PrintAnalysis(a string) {
	fmt.Printf("| %s:\n", a)
}

// PrintDiff prints on CLI a potential compat break : before VS after function, and an explanation
func PrintDiff(before, after string) {
	beforeFormatted := color.RedString("- " + before)
	fmt.Printf(" %s\n", beforeFormatted)
	if after != "" {
		afterFormatted := color.GreenString("+ " + after)
		fmt.Printf(" %s\n", afterFormatted)
	}
}

func PrintNewline() {
	fmt.Println()
}
