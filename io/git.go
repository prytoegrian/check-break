package io

import (
	"errors"
	"fmt"
	"os/exec"
	"strings"
)

const STATUS_FILE_POS = 0
const NAME_FILE_POS = 1

var ErrEmptyDiff = errors.New("empty diff between these two points")

// GitInteractor represents a tool able to interact w/ git
type GitInteractor interface {
	// NamesModified returns the modified files in the selected git diff
	NamesModified() ([]string, error)
	// NamesDeleted returns the deleted files in the selected git diff
	NamesDeleted() ([]string, error)
	// FileAtStartingPoint takes a filename and returns the file content at startpoint
	FileAtStartingPoint(string) (string, error)
	// FileAtEndingPoint takes a filename and returns the file content at endingPoint
	FileAtEndingPoint(string) (string, error)
}

type Git struct {
	startingPoint string
	endingPoint   string
	files         map[string][]string
}

func NewGit(s, e string) Git {
	return Git{
		startingPoint: s,
		endingPoint:   e,
		files:         make(map[string][]string),
	}
}

// TODO 2022-08-30: is a renamed file equivalent to modified ?
func (g Git) NamesModified() ([]string, error) {
	return g.namesWithFilter("M")
}

func (g Git) NamesDeleted() ([]string, error) {
	return g.namesWithFilter("D")
}

func (g Git) FileAtStartingPoint(filename string) (string, error) {
	return fileAtRevision(filename, g.startingPoint)
}

func (g Git) FileAtEndingPoint(filename string) (string, error) {
	return fileAtRevision(filename, g.endingPoint)
}

// namesWithFilter returns all changed files between two git revisions, fanned out by status
func (g Git) namesWithFilter(filter string) ([]string, error) {
	if len(g.files) == 0 {
		diffPattern := g.startingPoint + "..." + g.endingPoint
		cmd := fmt.Sprintf("git diff --name-status %s", diffPattern)
		names, err := run(cmd)
		if err != nil {
			return nil, err
		}
		if len(names) == 0 {
			return nil, ErrEmptyDiff
		}

		splitted := strings.Split(strings.TrimSpace(names), "\n")
		for _, row := range splitted {
			statusedFile := strings.Fields(row)
			status := statusedFile[STATUS_FILE_POS]
			name := statusedFile[NAME_FILE_POS]
			// There also are A, C, R, T, U, X, and B. UnHandled.
			// cf. https://git-scm.com/docs/git-git#Documentation/git-git.txt---git-filterACDMRTUXB82308203
			g.files[status] = append(g.files[status], name)
		}
	}

	return g.files[filter], nil
}

// ----------
// Unpointed
// ----------

// fileAtRevision returns file content at a specific git revision
func fileAtRevision(filename string, revision string) (string, error) {
	cmd := fmt.Sprintf("git show %s:%s", revision, filename)
	show, err := run(cmd)
	if err != nil {
		return "", err
	}

	return show, nil
}

func GuessMaxTag() (string, error) {
	cmd := "git tag --sort=-version:refname"
	tag, err := run(cmd)
	if err != nil {
		return "", err
	}
	splitted := strings.Split(tag, "\n")

	return splitted[0], nil
}

// run executes a CLI command (impure function !)
func run(cmd string) (string, error) {
	cmdArgs := strings.Fields(cmd)
	output, err := exec.Command(cmdArgs[0], cmdArgs[1:]...).Output()
	if err != nil {
		return "", err
	}

	return string(output), nil
}
