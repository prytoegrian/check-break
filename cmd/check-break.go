package main

import (
	"flag"
	"fmt"
	"log"
	"time"

	"gitlab.com/prytoegrian/check-break/analyser"
	"gitlab.com/prytoegrian/check-break/collector"
	"gitlab.com/prytoegrian/check-break/io"
	"gitlab.com/prytoegrian/check-break/parser"
)

func main() {
	firstTime := time.Now()
	startingPoint := flag.String("s", "", "Git starting point (it can be any git object): Defaulted to max git tag")
	endingPoint := flag.String("e", "HEAD", "Git ending point (it can be any git object)")
	verbose := flag.Bool("v", false, "Display timer, unparsed files and unchanged files")
	flag.Parse()
	if *startingPoint == "" {
		var err error
		*startingPoint, err = io.GuessMaxTag()
		if err != nil {
			log.Fatalf("can't guess max tag: %v", err)
		}
		fmt.Printf("Starting point is omitted, using max tag %v\n", *startingPoint)
	}
	// Let's go !
	git := io.NewGit(*startingPoint, *endingPoint)
	// 1. Collection step
	collector := collector.NewCollector(git)
	if err := collector.Collect(); err != nil {
		log.Fatalf("error during collection: %v", err)
	}
	collectedFiles := collector.Files()
	var durations []string
	durations = append(durations, fmt.Sprintf("After collection: %.2fs", time.Since(firstTime).Seconds()))
	// 2. Parsing step
	collectionParser := parser.NewCollectionParser()
	parsedFiles, emptyFiles, err := collectionParser.Parse(collectedFiles)
	if err != nil {
		log.Fatal(err)
	}
	durations = append(durations, fmt.Sprintf("After parse: %.2fs", time.Since(firstTime).Seconds()))
	// 3. Analysis step
	fileAnalyser := analyser.NewAnalyser()
	fileAnalyser.Analyse(parsedFiles)
	durations = append(durations, fmt.Sprintf("After analysis: %.2fs", time.Since(firstTime).Seconds()))
	// 4. Report step
	report(
		fileAnalyser.Files(),
		collectionParser.Unparseables(collectedFiles),
		emptyFiles,
		fileAnalyser.Unchanged(),
		*verbose,
		durations,
		len(collectedFiles),
	)
}

func report(analysed analyser.Files, unparsed collector.Files, emptyFiles parser.Files, unchanged analyser.Files, verbose bool, durations []string, numCollected int) {
	io.PrintTitle("Checkbreak")
	if verbose {
		io.PrintHeader()
	}
	io.PrintSection(fmt.Sprintf("%d changed files:", len(analysed)))
	for _, file := range analysed {
		io.PrintFile(file.Name())
		for analysis, rows := range file.Changes() {
			io.PrintAnalysis(string(analysis))
			for _, r := range rows {
				io.PrintDiff(r.Before(), r.After())
			}
		}
		io.PrintNewline()
	}
	if !verbose {
		return
	}
	io.PrintSection(fmt.Sprintf("%d ignored files (U: unparseable | P: no public functions | NC: no change):", len(unparsed)+len(emptyFiles)+len(unchanged)))
	for _, file := range unparsed {
		io.PrintFile(file.Name() + " [U]")
	}
	for _, file := range emptyFiles {
		io.PrintFile(file.Name() + " [P]")
	}
	for _, file := range unchanged {
		io.PrintFile(file.Name() + " [NC]")
	}

	// Verbose footer
	if verbose {
		fmt.Printf("\nFor %d collected files:", numCollected)
		fmt.Printf("\n%d diff, %d unparseable, %d private, %d unchanged\n", len(analysed), len(unparsed), len(emptyFiles), len(unchanged))
		fmt.Println(durations)
	}
}
