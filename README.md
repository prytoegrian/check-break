# Check-break
[![BCH compliance](https://bettercodehub.com/edge/badge/Prytoegrian/check-break?branch=master)](https://bettercodehub.com/)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/6f9a3e0c93ce4fb28c778019ad083179)](https://www.codacy.com/app/prytoegrian/check-break?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=Prytoegrian/check-break&amp;utm_campaign=Badge_Grade)

`check-break` helps you to discover compatibility breaks between two git revisions of your code, and to improve decisions-making to determine if a new major version is required. In few words, if you follow *semver* (or try to stick to it), you must use `check-break` ;-)

`check-break` itself follows `semver` and monitors its own changes. For now, it's under heavy development, use it at your own risks, compatibility breaks could happen at every moment.

System requirements: git, make and go env.

## What is a compatibility break ?
Basically, following the semver definition :
> A change is incompatible if it removes a possibility for the consumer in the public API.

All starts with a clear definition of **public API** in your context. Once done, a compatibility break occurs on all **public API** functions each time:
- a function is removed
- an argument is removed
- an argument is added (*without a default value*)
- a default argument is removed
- a default value is changed
- a return type is removed
- a return type is added
- type of any input / output / exception / assertion is changed and is incompatible with the former one (**1**)

**1.** In other words, if you're comfortable with [Liskov principle](https://en.wikipedia.org/wiki/Liskov_substitution_principle), you might have heard:
> Be contravariant in your preconditions, be covariant with your postconditions.

Thus, a compatibility break happens when you're covariant in your preconditions or contravariant in your postconditions.

Since `check-break` can't guess your public API (yet), it shows you all changes on public functions. It's up to you to determine if:
- this change really is a break,
- this change is in the public API scope.

## Usage

```sh
$ make
$ ./checkbreak [-s starting_point] [-e ending_point]
```

You may omit either points. In a such case, the starting point will be max git tag and the ending point will be git `HEAD`.

**Note:** With `-v` option, all unsupported files are reported as such, in order not to give a feeling of false negative.

## Languages supported

At this time, the following languages are supported:
- Golang

Feel free to participate to add yours, correct bugs, improve design, etc. `check-break` is under [GPL3](LICENCE).

Please remember that this tool may be incomplete, it doesn't replace the human judgment.


# TODO 

- highlight differences in output: https://neil.fraser.name/writing/diff/ / https://blog.jcoglan.com/2017/02/12/the-myers-diff-algorithm-part-1/
- add languages (cf. https://php-parser.com/, https://www.reddit.com/r/golang/comments/3lpn53/how_to_create_a_parser_in_go/, https://blog.gopheracademy.com/advent-2014/parsers-lexers/)
